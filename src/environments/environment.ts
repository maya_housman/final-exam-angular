// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyDcQlHErNCpMISxLyNUwYiv4n9h5oOP_tw",
    authDomain: "exam-maya.firebaseapp.com",
    projectId: "exam-maya",
    storageBucket: "exam-maya.appspot.com",
    messagingSenderId: "571535240000",
    appId: "1:571535240000:web:81c7dc9210b0aefd8928a5"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
