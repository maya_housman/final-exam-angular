import { AuthService } from './../auth.service';
import { StudentsService } from './../students.service';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'studentsform',
  templateUrl: './students-form.component.html',
  styleUrls: ['./students-form.component.css']
})
export class StudentsFormComponent implements OnInit {

    average_grade: number
  Psycho_grade: number


  paids:Object[] = [{id:1,value:'true'},{id:2,value:'false'}]
  paid:string;

  willbailOut:boolean = false;
  
  result: any;
  constructor(private studentsService: StudentsService, public authService:AuthService) { }

  userId:string;

  ngOnInit(): void {
    this.authService.getUser().subscribe(user => {
      this.userId = user.uid
    });
  }

  predict() {
    this.studentsService.predict(this.average_grade, this.Psycho_grade, this.paid).subscribe(result => {
      console.log(result);
      this.result = result;
      this.willbailOut = result.result <= 0.5;
    })
  }

  save() {
    this.studentsService.saveStudent(this.userId, {
        Paid: this.paid,
        Psycho_grade: this.Psycho_grade,
        average_grade: this.average_grade
    });
  }

  cancel() {
    this.average_grade = null;
    this.paid = null;
    this.Psycho_grade = null;
  }

}
