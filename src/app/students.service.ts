import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  constructor(private db:AngularFirestore,private http:HttpClient) { }

  private url = "https://41622g6vy0.execute-api.us-east-1.amazonaws.com/Beta" 
 

 

  studentCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users'); 

  average_grade: number;
  Psycho_grade: number;
  Paid: string;

  deleteStudent(Userid:string, id:string){
    this.db.doc(`users/${Userid}/students/${id}`).delete(); 
  } 

  saveStudent(Userid, student) {
    this.db.collection(`users/${Userid}/students`).add(student); 
  }

  predict( average_grade: number,
    Psycho_grade: number,
    Paid: string){
    let json = {
"average_grade":average_grade,
"Paid":Paid,
"Psycho_grade": Psycho_grade
}
let result = JSON.stringify(json);
return this.http.post<any>(this.url, result).pipe(
map(res => {
 console.log(res);
 let final:string = res.result;
 console.log(final);
 return final; 
})
)

}
 

  
  public getStudents(userId){
    this.studentCollection = this.db.collection(`users/${userId}/students`); 
    return this.studentCollection.snapshotChanges().pipe(map(
      collection =>collection.map(
        document => {
          const data = document.payload.doc.data(); 
          data.id = document.payload.doc.id;
          return data; 
        }
      )
    ))
    
  } }