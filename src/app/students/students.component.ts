import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Student } from '../interfaces/student';
import { StudentsService } from '../students.service';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {
  students$; 
  students:Student[];
  userId:string;
  editstate = [];
  average_grade: number;
  Psycho_grade: number;
  Paid: string;
  category:string;
  addCustomerFormOPen = false;
  

  constructor(private studentsService:StudentsService, public authService:AuthService) {
    this.authService.getUser().subscribe(user => {
      this.userId = user.uid
    });
   }

  deleteStudent(id:string){
    this.studentsService.deleteStudent(this.userId, id); 
  }

 





  ngOnInit(): void {
    //this.booksService.addBooks(); 
    //this.books$ = this.booksService.getBooks();
    //this.books$.subscribe(books => this.books = books);
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId); 
        this.students$ = this.studentsService.getStudents(this.userId);  
      }
    )

  }

}

